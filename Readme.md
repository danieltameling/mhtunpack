## MHTUNPACK

This project contains python code to extract the files contained in a .mht file. I wasn't able to get any of the existing solutions working, so I hacked together something. It often requires manual intervention (links don't point to the correct file / or multiple files with the same name, which I thought the user should at least look at.) but usually it gets the job done. So I hope this might be useful for anybody else.

If the code finds a Date: header in the mht file, it writes the timestamp to a file `timestamp.txt`. If there is no such header, it uses the mtime of the mht file for the timestamp. Thus one can execute something like
```
touch -d "$(cat timestamp.txt)" *
```
to change the timestamps of the extracted files to match the creation time of the mht file.
