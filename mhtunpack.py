#!/usr/bin/env python3

import argparse
import sys, os, itertools
from datetime import datetime, timezone
from email.parser import Parser
from urllib.parse import urlparse
import urllib

def replace_url_with_filename(key, newname, dictionary, dry_run):
    if dictionary[key]["binary"] == False:
        encoded = urllib.parse.quote_plus(newname)
        # the url replacing is probably incorrect. I'm not a web developer and don't know what I'm doing
        oldurl = dictionary[newname]["url"]
        # was required when src=... included a &
        # also required commenting out Renaming 3
        # oldurl = oldurl.replace("&", "&amp;")
        dictionary[key]["data"] = dictionary[key]["data"].replace(oldurl, encoded)
        if dry_run:
            print("Renaming 1: \"{}\" to \"{}\"".format(oldurl, encoded))
        # if oldurl is 'a.php' and encoded is 'a.php.html' then urlparse.path
        # is also 'a.php' and doing another replace would lead to 'a.php.html.html'
        parsed_url = urlparse(oldurl)
        if (path := parsed_url.path) != oldurl:
            if parsed_url.query != None:
                dictionary[key]["data"] = dictionary[key]["data"].replace(path + '?' + parsed_url.query, encoded)
                if dry_run:
                    print("Renaming 2: \"{}\" to \"{}\"".format(path + '?' + parsed_url.query, encoded))
            dictionary[key]["data"] = dictionary[key]["data"].replace(path, encoded)
            if dry_run:
                print("Renaming 3: \"{}\" to \"{}\"".format(path, encoded))

def mht_unpack(path, dry_run):
    filename = os.path.basename(path)
    if filename.endswith(".mht"):
        dirname = filename[:-4]
    else:
        dirname = "dir-" + filename
    try:
        output = {}
        with open(path) as f:
            data = f.read()
            p = Parser()
            msg = p.parsestr(data)
            date = msg.get("date")
            if date == None:
                st = os.stat(path)
                date = datetime.fromtimestamp(st.st_mtime, tz=timezone.utc).strftime('%a, %d %b %Y %H:%M:%S %z')
            for part in msg.walk():
                if part.is_multipart():
                    continue
                # print(part.get_content_type(), part.get_content_charset(), part.get("Content-Location"))
                filename = part.get_filename()
                url = part.get("Content-Location")
                if filename == None:
                    if url[-1] == '/':
                        filename = os.path.basename(url[:-1])
                    else:
                        filename = os.path.basename(url)
                        if filename == "":
                            print("Error: Empty filename from the Content-Location {}.".format(url))
                            sys.exit(1)
                    # had a .php file and that wouldn't display properly without the .html ending
                    if part.get_content_type() == "text/html" and not filename.endswith((".html", ".htm")):
                        filename += ".html"
                if dry_run:
                    print("File:", filename)
                if filename in output:
                    print("Error: Collision of filenames.\nWould write to file {} twice, loosing the first one.\nThe original path of the second occurrence is {}.".format(filename, part.get("Content-Location")))
                    sys.exit(1)
                content = part.get_payload(decode=1)
                if charset := part.get_content_charset():
                    output[filename] = {"binary": False, "url": url, "data": content.decode(charset), "encoding": charset}
                else:
                    output[filename] = {"binary": True, "url": url, "data":  content}
        for i in itertools.combinations(output, 2):
            replace_url_with_filename(i[0], i[1], output, dry_run)
            replace_url_with_filename(i[1], i[0], output, dry_run)

        if not dry_run:
            os.mkdir(dirname)
            for i in output:
                if output[i]["binary"] == False:
                    # with open(dirname + '/' + i, "w", encoding='utf-8') as f:
                    # with open(dirname + '/' + i, "w", encoding='windows-1252') as f:
                    with open(dirname + '/' + i, "w", encoding=output[i]["encoding"]) as f:
                        f.write(output[i]["data"])
                else:
                    with open(dirname + '/' + i, "wb") as f:
                        f.write(output[i]["data"])

            if date != None:
                timestamp_file = dirname + '/' + "timestamp.txt"
                if os.path.exists(timestamp_file):
                    print("Error: Cannot write timestamp.txt as it already exitsts.")
                    sys.exit(1)
                with open(timestamp_file, "w") as f:
                    f.write(date)
                    if not date.endswith("\n"):
                        f.write("\n")

    except IOError as err:
        print(err)
        print("failed to handle file", path)
    except:
        print("failed to handle file", path)
        raise

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extracts the files contained in mht files.')
    parser.add_argument('-o', '--output',
                        metavar='DIR',
                        type=str,
                        nargs=1,
                        help='output directory. If omitted it is the file name of the mht files without the .mht extension.')
    parser.add_argument('-d', '--dry-run',
                        help='list the files contained in the mht files and the renaming that would happen to keep links intact. Doesn\'t extract the files.',
                        action='store_true')
    parser.add_argument('files',
                        metavar='file.mht',
                        type=str,
                        help='the mht file',
                        nargs='+')

    args = parser.parse_args()
    dry_run = args.dry_run
    out_dir= args.output
    files=args.files

    for p in files:
        mht_unpack(p, dry_run)
